# _*_ coding:utf-8 _*_
#****************************************************************************************
# author:       Ahmed Mourad
# date:         10 June 2015
# description:  Question Classification
#****************************************************************************************
import argparse
import re, codecs, os, json, time
from collections import Counter
import cPickle as pickle
from itertools import izip

import numpy as np
from sklearn import metrics
from sklearn.svm import LinearSVC
from sklearn.feature_extraction import DictVectorizer
from sklearn.preprocessing import LabelEncoder
from sklearn import cross_validation

from nltk.tokenize import word_tokenize
from nltk.parse import stanford
from nltk.corpus import wordnet as wn

class QuestionClassifier(object):
    """Classifier for questions types"""

    # Regular expressions
    # WARNING: wh-word might not come at the beginning of sentence;
    # e.g. Material called linen is made from what plant?
    _WH_WORD = re.compile("^(?P<wh_word>what|which|when|where|who|how|why)\s.*$", re.IGNORECASE|re.UNICODE)

    # Rule-based question classification regular expressions
    # description definition pattern 1
    _DESC_DEF1 = re.compile("^what\s+(is|are)\s+(a|an|the)?\s*\w+\s*\w*\s*\?*$", re.IGNORECASE|re.UNICODE)
    # description definition pattern 2
    _DESC_DEF2 = re.compile("^what\s+(do|does).*mean\s*\?*$", re.IGNORECASE|re.UNICODE)
    # entity substance pattern
    _ENTY_SUBS = re.compile("^what\s+(is|are)\s+.*(composed of|made of|made out of)\s*\?*$", re.IGNORECASE|re.UNICODE)
    # description patttern
    _DESC_DESC = re.compile("^what\s+does\s+.*do\s*\?*$", re.IGNORECASE|re.UNICODE)
    # entity term pattern: asking for a term
    _ENTY_TERM = re.compile("^what\s+do\s+you\s+call.*$", re.IGNORECASE|re.UNICODE)
    # reason pattern 1
    _DESC_REA1 = re.compile("^what\s+(causes|cause).*$", re.IGNORECASE|re.UNICODE)
    # reason pattern 2
    _DESC_REA2 = re.compile("^what\s+(is|are).*used for\s*\?*$", re.IGNORECASE|re.UNICODE)
    # expression/abbreviation pattern
    _ABBR_EXPR = re.compile("^what\s+(does|do).*stand for\s*\?*$", re.IGNORECASE|re.UNICODE)
    # human description pattern. WARNING: two stage expression!
    _HUM_DESC = re.compile("^who\s+(is|was)\s+(\w+).*\s*\?*$", re.IGNORECASE|re.UNICODE)

    # Modified collins rules for determining question's headword; Silva et al.(2011)
    _HW_RULES = {
        'S':['VP', 'FRAG', 'SBAR', 'ADJP'],
        'SBARQ':['SQ', 'S', 'SINV', 'SBARQ', 'FRAG'],
        'SQ':['NP', 'VP', 'SQ'],
        'NP':['NX', 'NNS', 'NNPS', 'NNP', 'NN', 'NP'],
        'PP':['WHNP', 'NP', 'WHADVP', 'SBAR'],
        'WHNP':['NP'],
        'WHPP':['SBAR', 'NP', 'WHADVP', 'WHNP']
        }
    
    def __init__(self, model=None):
        self._clf = LinearSVC()
        self._encoder = LabelEncoder()
        self._vectorizer = DictVectorizer(dtype=float, sparse=True)

        # TODO: launch Stanford parser as a standalone server and replace the call with http request
        os.environ['STANFORD_PARSER'] = r'C:/Work/QuestionAnswering/question_classification/repo/resources/stanford-parser-full-2015-04-20/stanford-parser.jar'
        os.environ['STANFORD_MODELS'] = r'C:/Work/QuestionAnswering/question_classification/repo/resources/stanford-parser-full-2015-04-20/stanford-parser-3.5.2-models.jar'
        self._parser = stanford.StanfordParser(model_path=r'C:/Work/QuestionAnswering/question_classification/repo/resources/stanford-parser-full-2015-04-20/models/edu/stanford/nlp/models/lexparser/englishPCFG.ser.gz')

        if model:
            self.load(model)

    @staticmethod
    def _check_wh_word(sent):
        """
        Check if any question wh-word exists. Supporst 8 types:
        [what, which, when, where, who, how, why, rest]
        """
        wh_word = None
        m = re.match(QuestionClassifier._WH_WORD, sent)
        if m:
            wh_word = m.group('wh_word').lower()
        else:
            wh_word = 'wh_rest'
        
        return wh_word

    @staticmethod
    def _extract_head_word(tree):
        """Extract question's headword"""
        # stop condition: terminal node
        if tree.height() == 1:
            # return the head node (Tag HeadWord)
            return tree
        else:
            l = tree.label()
            # get priority list of traversing children nodes
            rules = QuestionClassifier._HW_RULES.get(l)
            
            # determine which child of tree is the head
            if rules:
                for r in rules:
                    for c in tree:
                        if c.height() > 1 and c.label() == r:
                            head_word = QuestionClassifier._extract_head_word(c)
                            if head_word:
                                return head_word
        return None
    
    def _classify_question_manually(self, sent, wh_word, tree):
        """Rule based question classification"""
        result = None
        if wh_word in ['when', 'where', 'why']:
            pass
        elif wh_word == 'how':
            # return the word after how
            result = sent.split(' ')[1]
        elif wh_word == 'what':
            if re.match(QuestionClassifier._DESC_DEF1, sent):
                result = 'DESC:def1'
            elif re.match(QuestionClassifier._DESC_DEF2, sent):
                result = 'DESC:def2'
            elif re.match(QuestionClassifier._ENTY_SUBS, sent):
                result = 'ENTY:subs'
            elif re.match(QuestionClassifier._DESC_DESC, sent):
                result = 'DESC:desc'
            elif re.match(QuestionClassifier._ENTY_TERM, sent):
                result = 'ENTY:term'
            elif re.match(QuestionClassifier._DESC_REA1, sent):
                result = 'DESC:rea1'
            elif re.match(QuestionClassifier._DESC_REA2, sent):
                result = 'DESC:rea2'
            elif re.match(QuestionClassifier._ABBR_EXPR, sent):
                result = 'ABBR:expr'
        elif wh_word == 'who' and re.match(QuestionClassifier._HUM_DESC, sent):
            r = re.match(QuestionClassifier._HUM_DESC, sent)
            # check if the word that follows starts with a capital letter
            if r.group(2)[0].isupper():
                result = 'HUM:desc'
        else:
            # extract head word from the question parse tree
            candidate = QuestionClassifier._extract_head_word(tree)
            head_word = ''
            
            if candidate and candidate[0].startswith('NN'):
                head_word = candidate[1]
            else:
                # extract the first word whose tag starts with NN; tree.pos
                pos = tree.pos()
                for word, tag in pos:
                    if tag.startswith('NN'):
                        head_word = word
                        break
            
            result = head_word.lower()
            
        return result

    @staticmethod
    def _extract_left_most_path(tree, tree_types=(list)):
        """Extract the left-most child path in the tree
        
        tree is represented in the form of nested lists
        as an output from Wordnet.synset.tree function
        """
        if isinstance(tree, tree_types):
            for value in tree[:2]:
                for subvalue in QuestionClassifier._extract_left_most_path(value):
                    yield subvalue
        else:
            yield tree
    
    @staticmethod
    def _resolve_head_word_semantics(tokens, head_word, tree):
        """Extract hypernyms of the head word based on WordNet"""
        count = 0
        maxCount = -1
        optimum_sense = None
        hypernyms = []
        
        # retrieve pairs of word, pos-tag
        pos = tree.pos()
        
        h_index = next(i for i, (w, p) in enumerate(pos) if w.lower() == head_word)
        head_word = pos[h_index]
        del pos[h_index]

        # map Penn Treebank POS tags to Wordnet POS tags
        pos_map = {'NN':'n', 'NNS':'n', 'NNP':'n', 'NNPS':'n',
                   'JJ':'a', 'JJR':'a', 'JJS':'a',
                   'RB':'r', 'RBR':'r', 'RBS':'r',
                   'VB':'v', 'VBD':'v', 'VBG':'v', 'VBN':'v', 'VBP':'v', 'VBZ':'v'}
        
        # loop over senses of the head word
        for s in wn.synsets(head_word[0], pos=pos_map.get(head_word[1])):
            # retrieve definition of the current sense, tokenize and convert to set
            sdef = set(word_tokenize(s.definition()))
            count = 0
            
            # loop over context words in the question except the head word
            for w, t in pos:
                overlap = []
                for ws in wn.synsets(w, pos=pos_map.get(t)):
                    wdef = set(word_tokenize(ws.definition()))
                    # calculate words overlap between s & t definitions
                    overlap.append(len(sdef.intersection(wdef)))

                if len(overlap) > 0:
                    count += max(overlap)
            
            if count > maxCount:
                maxCount = count
                optimum_sense = s

        if optimum_sense:
            # extract hypernyms of the optimum sense
            hypernyms = optimum_sense.tree(lambda h:h.hypernyms(), depth=6)
            # extract the left-most child path only
            senses = list(QuestionClassifier._extract_left_most_path(hypernyms))
            hypernyms = []
            for s in senses:
                hypernyms.append(s.name().split('.')[0].replace('_', ' '))

        return hypernyms

    @staticmethod
    def _generate_word_ngrams(sent):
        """Split question into sequences of n-grmas; initially unigrams only"""
        return word_tokenize(sent)

    @staticmethod
    def _extract_word_shape(tokens):
        """
        Supports 5 word shape features:
        {au:all_upper_case, al:all_lower_case, mc:mixed_case, ad:all_digits, ot:other}
        """
        result = Counter()
        for t in tokens:
            if t.islower():
                result['ws_all_lower'] += 1
            elif t.isupper():
                result['ws_all_upper'] += 1
            elif t.istitle():
                result['ws_mixed_case'] += 1
            elif t.isdigit():
                result['ws_all_digits'] += 1
            else:
                result['ws_other'] += 1

        return result
    
    def extract_features(self, questions):
        """Extract features."""
        # List of dictionaries {feature:value} per question
        X = []
        start_extr = time.time()
        for q in questions:
            tokens = [t.lower() for t in self._generate_word_ngrams(q)]
            features = dict()
            
            # wh-word feature
            wh_word = self._check_wh_word(q)
            features.update({'wh_word':wh_word})
            
            # unigram features
            features.update(Counter(tokens))
            
            # word shape features
            features.update(self._extract_word_shape(tokens))
            
            # classify question manually or extract head word
            # generate syntactic parse tree; skip ROOT
            
            tree = self._parser.raw_parse(q).next()[0]
            
            qc = self._classify_question_manually(q, wh_word, tree)
            if qc:
                features.update({'head_word':qc})
            
            # resolve head word ambiguity if exists
            if qc and qc.islower():
                hypernyms = self._resolve_head_word_semantics(tokens, qc, tree)
                features.update(dict.fromkeys(hypernyms, True))

            X.append(features)

        end_extr = time.time()
        return X
    
    def train(self, X, y):
        """Train question classification model"""
        X = self._vectorizer.fit_transform(X)
        y = self._encoder.fit_transform(y)
        self._clf.fit(X, y)
        return self
        
    def classify(self, testdataset):
        """Classify a set of questions"""
        X = self._vectorizer.transform(testdataset)
        y = self._clf.predict(X)
        return self._encoder.inverse_transform(y)
    
    def evaluate(self, y_true, y_pred):
        """Evaluate question classification model"""
        # separate coarse and fine labels
        cfy_true, cfy_pred = [], []
        for yt, yp in izip(y_true, y_pred):
            cfy_true.append(tuple(yt.split(':')))
            cfy_pred.append(tuple(yp.split(':')))
        
        # accuracy per feature group and taxonomy, precision & recall per class
        coarse_score = metrics.accuracy_score([y[0] for y in cfy_true], [y[0] for y in cfy_pred])
        fine_score = metrics.accuracy_score([y[1] for y in cfy_true], [y[1] for y in cfy_pred])
        class_report = metrics.classification_report(y_true, y_pred)

        results_filepath = os.path.dirname(os.path.abspath(__file__))+'/data/eval_results.txt'
        with codecs.open(results_filepath, 'w', 'utf-8') as results_file:
            results_file.write('Accuracy for Coarse (6) classes\t{}\nAccuracy for Fine (50) classes\t{}\n\n'.format(str(coarse_score), str(fine_score)))
            results_file.write('Classification report:\n'+class_report+'\n\n')
    
    def cross_validate(self, feats, labels, n_folds):
        """Stratified n-fold cross validation based on accuracy ONLY to be used for development"""
        X = self._vectorizer.fit_transform(feats)
        y = self._encoder.fit_transform(labels)

        # calculate accuracy for the Fine (50) taxonomy
        fine_score = cross_validation.cross_val_score(self._clf, X, y, cv=n_folds, n_jobs=1).mean()
        
        # calculate accuracy for the Coarse (6) taxonomy
        coarse_labels = [l.split(':')[0] for l in labels]
        coarse_y = self._encoder.fit_transform(coarse_labels)
        self._clf = LinearSVC()
        coarse_score = cross_validation.cross_val_score(self._clf, X, coarse_y, cv=n_folds, n_jobs=1).mean()
        
        return fine_score, coarse_score
    
    def save(self):
        """Save question classification model"""
        filepath = os.path.dirname(os.path.abspath(__file__))+'/model/qc_model.pickle'
        with codecs.open(filepath, 'wb') as classif_file, \
             codecs.open(filepath+'.features', 'wb') as vectorizer_file, \
             codecs.open(filepath+'.classes', 'wb') as encoder_file:
                pickle.dump(self._liw_classifier, classif_file, pickle.HIGHEST_PROTOCOL)
                pickle.dump(self._vectorizer, vectorizer_file, pickle.HIGHEST_PROTOCOL)
                pickle.dump(self._encoder, encoder_file, pickle.HIGHEST_PROTOCOL)
        
    def load(self, model):
        """Load question classification model"""
        with codecs.open(model, 'rb') as classif_file, \
             codecs.open(model+'.features', 'rb') as vectorizer_file, \
             codecs.open(model+'.classes', 'rb') as encoder_file:
            self._clf = pickle.load(classif_file)
            self._vectorizer = pickle.load(vectorizer_file)
            self._encoder = pickle.load(encoder_file)

    @staticmethod
    def save_features(featdataset, filepath):
        """Save features to a json file"""
        with codecs.open(filepath, 'w', 'utf-8') as outfile:
            json.dump(featdataset, outfile, ensure_ascii=False, indent=4, separators=(',', ':'))

if __name__=="__main__":
    clf = QuestionClassifier()
    
    def load(filepath):
        """
        Load input file with the following format per line
        coarse_category:fine_category question_text
        """
        X, y = [], []
        with codecs.open(filepath, 'r') as inputfile:
            for i, line in enumerate(inputfile):
                try:
                    label, question = line.split(' ', 1)
                    X.append(question)
                    y.append(label)
                except UnicodeDecodeError:
                    print i

        return X, y

    def cross_validate(args):
        X, y = load(args.input)
        X = clf.extract_features(X)
        clf.save_features(X, args.input+'.feats.json')
        fine_score, coarse_score = clf.cross_validate(X, y, args.folds)
        print 'Accuracy for Coarse (6) classes\t{}\nAccuracy for Fine (50) classes\t{}'.format(str(coarse_score), str(fine_score))

    def train(args):
        X, y = load(args.input)
        X = clf.extract_features(X)
        clf.save_features(X, args.input+'.feats.json')
        # train
        clf.train(X, y)
        # test
        if args.test:
            X, yt = load(args.test)
            X = clf.extract_features(X)
            yp = clf.classify(X)
            clf.evaluate(yt, yp)
        
    parser = argparse.ArgumentParser(prog="Question Classification")
    subparsers = parser.add_subparsers()

    parser_cross = subparsers.add_parser('cross_validate', help='Evaluate the training model using n-folds cross validation.')
    parser_cross.add_argument('input', type=str, help='training filepath')
    parser_cross.add_argument('-n', '--folds', type=int, choices=range(2,11), default=10, help='number of folds, should be at least 2')
    parser_cross.set_defaults(func=cross_validate)

    parser_train = subparsers.add_parser('train', help='Train and evaluate a question classification model.')
    parser_train.add_argument('input', type=str, help='training filepath')
    parser_train.add_argument('-t', '--test', type=str, help='testing dataset')
    parser_train.add_argument('-s', '--save', action='store_true', help='save generated model to passed filepath')
    parser_train.add_argument('-m', '--most_informative', action='store_true', help='generate most informative features')
    parser_train.set_defaults(func=train)
    
    args = parser.parse_args()
    args.func(args)








