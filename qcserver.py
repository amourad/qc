# _*_ coding:utf-8 _*_
#****************************************************************************************
# author:       Ahmed Mourad
# date:         17 August 2015
# description:  Question Classification Standalone Server
#****************************************************************************************
import pyjsonrpc
from qc import QuestionClassifier

class RequestHandler(pyjsonrpc.HttpRequestHandler):
    def __init__(self, model):
        self.qclf = QuestionClassifier(model)
    
    @pyjsonrpc.rpcmethod
    def classify(self, questions):
        """Classify Questions
        questions: list of input questions
        """
        X = self.qclf.extract_features(questions)
        y = self.qclf.classify(X)
        return y

    @pyjsonrpc.rpcmethod
    def extract_headchunks(self, questions):
        """Extract head chunks
        questions: list of questions
        """
        pass

if __name__ == "__main__":
    # Threading HTTP-Server
    http_server = pyjsonrpc.ThreadingHttpServer(
        server_address = ('localhost', 8001),
        RequestHandlerClass = RequestHandler
    )
    print "Starting Question Classification HTTP server ..."
    print "URL: http://localhost:8001"
    http_server.serve_forever()