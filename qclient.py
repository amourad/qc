# _*_ coding:utf-8 _*_
#****************************************************************************************
# author:       Ahmed Mourad
# date:         21 August 2015
# description:  Question Classification Client
#****************************************************************************************
import pyjsonrpc

if __name__ == "__main__":
    # test server
    http_client = pyjsonrpc.HttpClient(url = "http://localhost:8001")
    print 'What is an atom?'
    print http_client.classify(['What is an atom?'])
    print http_client.extract_headchunks(['What is an atom?'])