# _*_ coding:utf-8 _*_
#****************************************************************************************
# author:       Ahmed Mourad
# date:         28 August 2015
# description:  Head Word Extraction Unit Test
#****************************************************************************************

from qc import QuestionClassifier

clf = QuestionClassifier()
tree = clf._parser.raw_parse('What county is Modesto, California in?')[0][0]
print QuestionClassifier.extract_head_word(tree)

clf = QuestionClassifier()
tree = clf._parser.raw_parse('Who was Galileo?')[0][0]
print QuestionClassifier.extract_head_word(tree)

clf = QuestionClassifier()
tree = clf._parser.raw_parse('What is an atom?')[0][0]
print QuestionClassifier.extract_head_word(tree)

clf = QuestionClassifier()
tree = clf._parser.raw_parse('What is the name of the chocolate company in San Francisco?')[0][0]
print QuestionClassifier.extract_head_word(tree)

clf = QuestionClassifier()
tree = clf._parser.raw_parse('George Bush purchased a small interest in which baseball team?')[0][0]
print QuestionClassifier.extract_head_word(tree)

clf = QuestionClassifier()
tree = clf._parser.raw_parse('What is Australia\'s national flower?')[0][0]
print QuestionClassifier.extract_head_word(tree)

clf = QuestionClassifier()
tree = clf._parser.raw_parse('Why does the moon turn orange?')[0][0]
print QuestionClassifier.extract_head_word(tree)

clf = QuestionClassifier()
tree = clf._parser.raw_parse('What is autism?')[0][0]
print QuestionClassifier.extract_head_word(tree)

clf = QuestionClassifier()
tree = clf._parser.raw_parse('What city had a world fair in 1900?')[0][0]
print QuestionClassifier.extract_head_word(tree)

clf = QuestionClassifier()
tree = clf._parser.raw_parse('What is the average weight of a Yellow Labrador?')[0][0]
print QuestionClassifier.extract_head_word(tree)

clf = QuestionClassifier()
tree = clf._parser.raw_parse('Who was the first man to fly across the Pacific Ocean?')[0][0]
print QuestionClassifier.extract_head_word(tree)

clf = QuestionClassifier()
tree = clf._parser.raw_parse('What day and month did John Lennon die?')[0][0]
print QuestionClassifier.extract_head_word(tree)

clf = QuestionClassifier()
tree = clf._parser.raw_parse('What is the life expectancy for crickets?')[0][0]
print QuestionClassifier.extract_head_word(tree)

clf = QuestionClassifier()
tree = clf._parser.raw_parse('What metal has the highest melting point?')[0][0]
print QuestionClassifier.extract_head_word(tree)

clf = QuestionClassifier()
tree = clf._parser.raw_parse('Who developed the vaccination against polio?')[0][0]
print QuestionClassifier.extract_head_word(tree)

clf = QuestionClassifier()
tree = clf._parser.raw_parse('What is epilepsy?')[0][0]
print QuestionClassifier.extract_head_word(tree)

clf = QuestionClassifier()
tree = clf._parser.raw_parse('What year did the Titanic sink?')[0][0]
print QuestionClassifier.extract_head_word(tree)

clf = QuestionClassifier()
tree = clf._parser.raw_parse('What is a biosphere?')[0][0]
print QuestionClassifier.extract_head_word(tree)

clf = QuestionClassifier()
tree = clf._parser.raw_parse('What river in the US is known as the Big Muddy?')[0][0]
print QuestionClassifier.extract_head_word(tree)

clf = QuestionClassifier()
tree = clf._parser.raw_parse('What is the capital of Yugoslavia?')[0][0]
print QuestionClassifier.extract_head_word(tree)

clf = QuestionClassifier()
tree = clf._parser.raw_parse('What country are Godiva chocolates from ?')[0][0]
print QuestionClassifier.extract_head_word(tree)

clf = QuestionClassifier()
tree = clf._parser.raw_parse('What country\'s capital is Tirana?')[0][0]
print QuestionClassifier.extract_head_word(tree)

clf = QuestionClassifier()
tree = clf._parser.raw_parse('What kind of animal is Babar?')[0][0]
print QuestionClassifier.extract_head_word(tree)